$fn=180;

difference() {
    union() {
        translate([0,0,15/2]) cube([82, 56, 15], center=true);
        translate([76/2,0,15]) hull() {
            translate([0,(52-10)/2,0]) rotate([0,90,0]) cylinder(h=2, d=10, center=true);
            translate([0,-(52-10)/2,0]) rotate([0,90,0]) cylinder(h=2, d=10, center=true);
        }     
        translate([-76/2,0,15]) hull() {
            translate([0,(52-10)/2,0]) rotate([0,90,0]) cylinder(h=2, d=10, center=true);
            translate([0,-(52-10)/2,0]) rotate([0,90,0]) cylinder(h=2, d=10, center=true);
        }     
    }
    union() {
        translate([0,0,2+13/2]) cube([62, 52, 13], center=true);
        translate([0,0,2+13/2]) cube([74, 42, 13], center=true);
        translate([0,0,12+3/2]) cube([70, 50, 3], center=true);
        #translate([82/2+10/2-21,-56/2,13.5+1.5/2]) cube([10, 5, 1.5], center=true);
        #translate([82/2+7/2-19,56/2,13.5+1.5/2]) cube([7, 5, 1.5], center=true);
    }
}