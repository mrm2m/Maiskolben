$fn=180;

translate([0,0,10]) {
    rotate([180,0,0]) {
        difference() {
            union() {
                translate([-41,-67/2,0]) cube([82,67,10]);
            }
            union() {
                translate([0,-63/2,0]) {
                    translate([-74/2,0,0]) cube([74,63,4]);
                    translate([-5,0,0]) {
                        translate([-78/2+11.5,21.5,0]) cube([50,36,8]);
                        translate([-36/2-6,25,0]) cube([36,30,10]);
                        translate([12+20,25+30-10,0]) cylinder(10,d=11);
                        translate([12+20,25+30-10-5*2.54,0]) cylinder(10,d=11);
                        translate([12+20,25+30-10-11*2.54,0]) cylinder(10,d=11);
                        translate([12+20,25+30-10,0]) cylinder(9,d=13);
                        translate([12+20,25+30-10-5*2.54,0]) cylinder(9,d=13);
                        translate([12+20,25+30-10-11*2.54,0]) cylinder(9,d=13);
                        translate([-3,0,0]) {
                            translate([12+20-9*2.54,25+30-10-14*2.54,0]) cylinder(10,d=11);
                            translate([12+20-14*2.54,25+30-10-14*2.54,0]) cylinder(10,d=11);
                            translate([12+20-19*2.54,25+30-10-14*2.54,0]) cylinder(10,d=11);
                            translate([12+20-9*2.54,25+30-10-14*2.54,0]) cylinder(9,d=13);
                            translate([12+20-14*2.54,25+30-10-14*2.54,0]) cylinder(9,d=13);
                            translate([12+20-19*2.54,25+30-10-14*2.54,0]) cylinder(9,d=13);
                        }
                    }
                }
            }
        }
        translate([-76/2,0,0]) hull() {
            translate([0,-(63-10)/2,0]) rotate([0,90,0]) cylinder(h=2, d=10, center=true);
            translate([0,(63-10)/2,0]) rotate([0,90,0]) cylinder(h=2, d=10, center=true);
        }        
        translate([76/2,0,0]) hull() {
            translate([0,-(63-10)/2,0]) rotate([0,90,0]) cylinder(h=2, d=10, center=true);
            translate([0,(63-10)/2,0]) rotate([0,90,0]) cylinder(h=2, d=10, center=true);
        }        
    }
}
        
