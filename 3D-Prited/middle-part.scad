$fn=180;

translate([0,0,37.5]) rotate([180+30,0,0]) difference() {
    union() {
        hull() {
            translate([0,-54/2,1]) cube([82, 2, 2], center=true);
            translate([0,-54/2,11]) cube([82, 2, 2], center=true);
        }
        
        hull() {
            translate([0,54/2,1]) cube([82, 2, 2], center=true);
            translate([0,54/2,11]) cube([82, 2, 2], center=true);
        }
        
        hull() {
            translate([40,-54/2,6]) cube([2, 2, 12], center=true);
            translate([40, 54/2,6]) cube([2, 2, 12], center=true);
        }
        hull() {
            translate([-40,-54/2,6]) cube([2, 2, 12], center=true);
            translate([-40, 54/2,6]) cube([2, 2, 12], center=true);
        }

        hull() {
            translate([0,54/2,11]) cube([82, 2, 2], center=true);
            translate([0,6,40]) rotate([-30,0,0]) translate([0,65/2,-9]) cube([82, 2, 2], center=true);
        }

        hull() {
            translate([0,-54/2,11]) cube([82, 2, 2], center=true);
            translate([0,6,40]) rotate([-30,0,0]) translate([0,-65/2,-9]) cube([82, 2, 2], center=true);
        }

        hull() {
            translate([-40,54/2,11]) cube([2, 2, 2], center=true);
            translate([-40,6,40]) rotate([-30,0,0]) translate([0,65/2,-9]) cube([2, 2, 2], center=true);
            translate([-40,-54/2,11]) cube([2, 2, 2], center=true);
            translate([-40,6,40]) rotate([-30,0,0]) translate([0,-65/2,-9]) cube([2, 2, 2], center=true);
        }

        hull() {
            translate([40,54/2,11]) cube([2, 2, 2], center=true);
            translate([40,6,40]) rotate([-30,0,0]) translate([0,65/2,-9]) cube([2, 2, 2], center=true);
            translate([40,-54/2,11]) cube([2, 2, 2], center=true);
            translate([40,6,40]) rotate([-30,0,0]) translate([0,-65/2,-9]) cube([2, 2, 2], center=true);
        }

        #translate([0,6,40]) rotate([-30,0,0]) {
            hull() {
                translate([0,-65/2,-1]) cube([82, 2, 2], center=true);
                translate([0,-65/2,-9]) cube([82, 2, 2], center=true);
            }
            hull() {
                translate([0,65/2,-1]) cube([82, 2, 2], center=true);
                translate([0,65/2,-9]) cube([82, 2, 2], center=true);
            }
            hull() {
                translate([40,-65/2,-5]) cube([2, 2, 10], center=true);
                translate([40, 65/2,-5]) cube([2, 2, 10], center=true);
            }
            hull() {
                translate([-40,-65/2,-5]) cube([2, 2, 10], center=true);
                translate([-40, 65/2,-5]) cube([2, 2, 10], center=true);
            }
        }
    }
    union() {
#        translate([82/2+10/2-21,-56/2,11.5/2]) cube([10, 5, 11.5], center=true);
        #translate([82/2+10/2-53,-56/2,4]) cube([10, 5, 5], center=true);
        translate([82/2+7/2-19,56/2,5/2]) cube([7, 5, 5], center=true);
    }
}

//}